//Initialisation de la liste
tabTasks = [
];

console.log(tabTasks);

function AddToTheList() {
    //R�cup�rer les valeurs des inputs
    text = document.getElementById("textbox").value;
    quantite = document.getElementById("quantitebox").value;
    prix = document.getElementById("prixbox").value;

    //ajouter les valeurs dans l'array
    tabTasks.push([text, quantite, prix, quantite * prix]);

    //Updtade de la liste
    console.log(tabTasks);
    updateTaskList();

    //Evite de recharger la page.
    return false;
}

//fonctions

//Calculer le prix par ligne en fonction de la ligne donn�.
function calcul(i) {
    tabTasks[i][3] = tabTasks[i][1] * tabTasks[i][2];
}

//Suprime une ligne
function deleteTask(clicked_id)
{
	var deletedId = Number(clicked_id);

	console.log(deletedId);

	//Suprimer de l'array
	tabTasks.splice(deletedId, 1);
	
	//Debug
	console.log(tabTasks);

    //update
	updateTaskList();
}

//update la liste
function updateTaskList()
{
	var list = document.querySelector("#mainList");
	
	//D�truire tous les �l�ments.
	var child = list.lastElementChild;
	while(child)
	{
		list.removeChild(child);
		child = list.lastElementChild;
	}

	//On cr�er de nouveau toutes les t�ches
	for(var i = 0; i < tabTasks.length; i++)
	{
	    //Cr�ation du truc joli
	    var inputDiv = document.createElement("div");
	    inputDiv.classList.add("input-group");
	    inputDiv.classList.add("mb-1");

	    //Cr�ation des inputs
	    var inputName = document.createElement("input");
	    inputName.type = "text";
	    inputName.classList.add("form-control");
	    inputName.value = tabTasks[i][0];
	    inputName.placeholder = "Nom du produit...";
	    inputName.id = "Nomuuuuu" + i;
	    inputName.onchange = function () { Recalcul(this.id, 0) }
	    inputDiv.appendChild(inputName);

	    var inputQuantity = document.createElement("input");
	    inputQuantity.type = "number";
	    inputQuantity.classList.add("form-control");
	    inputQuantity.value = tabTasks[i][1];
	    inputQuantity.placeholder = "Quantit�e...";
	    inputQuantity.id = "Quantity" + i;
	    inputQuantity.onchange = function () { Recalcul(this.id, 1) }
	    inputDiv.appendChild(inputQuantity);

	    var inputPrice = document.createElement("input");
	    inputPrice.type = "number";
	    inputPrice.classList.add("form-control");
	    inputPrice.value = tabTasks[i][2];
	    inputPrice.placeholder = "Prix unitaire...";
	    inputPrice.id = "Priceuuu" + i;
	    inputPrice.onchange = function () { Recalcul(this.id, 2) }
	    inputDiv.appendChild(inputPrice);

	    var inputGroupDiv = document.createElement("div");
	    inputGroupDiv.classList.add("input-group-append")
	    inputDiv.appendChild(inputGroupDiv);

	    var totalUnitPrice = document.createElement("span");
	    totalUnitPrice.classList.add("input-group-text");
	    calcul(i);
	    totalUnitPrice.appendChild(document.createTextNode(tabTasks[i][3] + " $"))
	    inputGroupDiv.appendChild(totalUnitPrice);

	    var checkBoxDiv = document.createElement("div");
	    checkBoxDiv.classList.add("input-group-text");
	    inputGroupDiv.appendChild(checkBoxDiv);

	    var checkbox = document.createElement("input");
	    checkbox.type = "checkbox";
	    checkBoxDiv.appendChild(checkbox);

	    var buttonSupr = document.createElement("button");
	    buttonSupr.classList.add("btn");
	    buttonSupr.classList.add("btn-outline-danger");
	    buttonSupr.type = "button";
	    buttonSupr.id = i;
	    buttonSupr.onclick = function () { deleteTask(this.id) };
	    buttonSupr.appendChild(document.createTextNode("Supprimer"));
	    inputGroupDiv.appendChild(buttonSupr);


			//Ajoute les listes dans l'html
		var theList = document.getElementById("mainList");
		theList.appendChild(inputDiv);

		console.log(tabTasks);
	}

    //calcul du prix total de la liste.
	var prixTotal = 0;
	for (var i = 0; i < tabTasks.length; i++) {
	    prixTotal = prixTotal + tabTasks[i][3];
	    console.log(prixTotal);
	}
	var t = document.createElement("div")
	t.classList.add("col-12");
	t.appendChild(document.createTextNode("PRIX TOTAL = " + prixTotal));
	inputDiv.appendChild(t);
}

//Recalcul la ligne et update la liste.
function Recalcul(id, column) {

    var chose = document.getElementById(id)
    var realId = id.substring(8);

    tabTasks[realId][column] = chose.value;

    calcul(realId);

    updateTaskList();
}